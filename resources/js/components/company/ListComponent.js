import React, { Component } from 'react';
import axios from 'axios';
export default class ListComponent extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            data: {
                data: []
            },
            rangeWithDots: [],
            current_page: 1,
            sort_column: 'id',
            direction: 'desc',
            per_page: 10
        });
        this.handleChangePage = this.handleChangePage.bind(this);
        this.handlePrev = this.handlePrev.bind(this);
        this.handleNext = this.handleNext.bind(this);
    }

    componentDidMount() {
        this.fetchData();
    }
    fetchData() {
        axios.get(this.buildUrl())
            .then(res => {
                let result = res.data;
                let current_page = result.data.current_page;
                let last_page = result.data.last_page;
                this.setState({
                    data: result.data,
                    rangeWithDots: this.pagination(current_page, last_page),
                    current_page: current_page
                })
            })
            .catch(err => console.log(err))
    }

    handleNext() {
        this.state.current_page++;
        this.fetchData();
    }
    handlePrev() {
        this.state.current_page--;
        this.fetchData();
    }

    pagination(c, m) {// c = current_page, m = sum page
        let delta = 2,
            range = [],
            rangeWithDots = [],
            l;
        range.push(1)
        for (let i = c - delta; i <= c + delta; i++) {
            if (i < m && i > 1) {
                range.push(i);
            }
        }
        if (m > 1) {
            range.push(m);
        }
        range.map(val => {
            if (l) {
                if (val - l === 2) {
                    rangeWithDots.push(l + 1);
                } else if (val - l !== 1) {
                    rangeWithDots.push('...');
                }
            }
            rangeWithDots.push(val);
            l = val;
        });
        return rangeWithDots;
    }

    handleChangePage(page) {
        if(page === '...') return false;
        this.state.current_page = page;
        this.fetchData();
    }

     handleSort(column) {
         this.state.sort_column = column;
         if(this.state.direction === 'desc') {
             this.state.direction = 'asc'
         } else {
             this.state.direction = 'desc'
         }
         this.fetchData();
     }

     buildUrl() {
         let s = this.state;
         return `/api/company?page=${s.current_page}&sort_column=${s.sort_column}&direction=${s.direction}&per_page=${s.per_page}`;
     }
    render() {
        const items = this.state.data.data;
        const rangeWithDots = this.state.rangeWithDots;
        const elmPagination = rangeWithDots.map( (val, index) => {
            return (
                <li key={index} className={this.state.current_page === val ? 'active' : ''}>
                    <a onClick={() => this.handleChangePage(val)}>{val}</a>
                </li>
            )
        });
        const elmItem = items.map((item,index) => {
                        return (
                            <tr key={index}>
                                <td>{item.id}</td>
                                <td>{item.name}</td>
                                <td>{item.email}</td>
                                <td>{item.phone}</td>
                                <td>{item.address}</td>
                                <td>{item.created_at}</td>
                            </tr>
                        )
                    });

        const theads = [
            {title: 'ID', key: 'id', sort: true},
            {title:'Name', key: 'name', sort: true},
            {title: 'Email', key: 'email', sort: true},
            {title: 'Phone', key: 'phone', sort: true},
            {title: 'Address', key: 'address', sort: true},
            {title: 'Created_At', key: 'created_at', sort: false}
        ];

        let elmThead = theads.map( (item, index) => {
            if(item.sort === true) {
                if(this.state.sort_column === item.key) {
                    return (
                        <th key={index} onClick={() => this.handleSort(item.key)}>
                            {item.title}
                            <span
                                className={this.state.direction === 'desc' ?
                                    'glyphicon glyphicon-sort-by-attributes-alt' :
                                    'glyphicon glyphicon-sort-by-attributes'}>
                            </span>
                        </th>
                    )
                } else {
                    return (
                        <th key={index} onClick={() => this.handleSort(item.key)}>
                            {item.title}
                        </th>
                    )
                }
            } else {
                return (
                    <th key={index}>{item.title}</th>
                )
            }
        });
        return (
            <div className="row">
                <div className="col-md-12 col-xs-12">
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <span className="panel-title">
                                Company
                            </span>
                        </div>
                        <div className="panel-body">
                            <table className="table table-hover">
                                <thead>
                                    <tr>
                                        {elmThead}
                                    </tr>
                                </thead>
                                <tbody>
                                    { elmItem }
                                </tbody>
                            </table>
                        </div>
                        <div className="panel-footer">
                            <nav aria-label="...">
                                <ul className="pagination">
                                    <li className={this.state.data.prev_page_url === null ? 'disabled' : ''}>
                                        <a onClick={this.handlePrev}>&laquo;</a>
                                    </li>
                                        {elmPagination}
                                    <li className={this.state.data.next_page_url === null ? 'disabled' : ''}>
                                        <a onClick={this.handleNext}>&raquo;</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}