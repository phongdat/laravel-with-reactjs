import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import ListComponent from './company/ListComponent'
export default class Root extends Component {
    render() {
        return (
            <div className="container">
                <ListComponent />
            </div>
        );
    }
}

if (document.getElementById('root')) {
    ReactDOM.render(<Root />, document.getElementById('root'));
}
