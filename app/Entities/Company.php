<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Support\DataTablePaginate;
class Company extends Model
{
    use DataTablePaginate;
    protected $table = 'companies';
    protected $fillable = ['email', 'name', 'phone', 'address'];
    protected $hidden = [];
}
