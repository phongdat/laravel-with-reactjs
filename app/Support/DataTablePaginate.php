<?php
namespace App\Support;

trait DataTablePaginate {

    public function scopeDataTablePaginate($query) {
        $request = request();
        $sort_column = 'id';
        $direction = 'desc';
        $per_page = 10;
        if ($request->sort_column !== null) $sort_column = $request->sort_column;
        if ($request->direction !== null) $direction = $request->direction;
        if ($request->per_page !== null) $per_page = $request->per_page;
        return $query->orderBy($sort_column, $direction)
            ->paginate($per_page);
    }

}